import React from 'react'
import {Form, Row, Col, Button, Card, Container} from 'react-bootstrap'



function update() {

    








    return (
        <React.Fragment>
            <Container className= "w-50">
            <Card>
                <Card.Body>
                <Form>
                    <Form.Group>
                        <Form.Label>Category Type</Form.Label>
                        <Form.Control as="select" required onChange={e => setCategoryType(e.target.value)}>
                        <option default>Select Category Type</option>
                        <option value= "Income">Income</option>
                        <option value = "Expenses">Expenses</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                        <Form.Label>Category Name</Form.Label>
                        <Form.Control as="select" onChange={e => setCategoryName (e.target.value)}>
                        <option default>Select Category Name</option>
                            
                        </Form.Control>
                    </Form.Group>

                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Amount</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder="Enter Amount" 
                        
                        onChange= {e => setAmount(e.target.value)}
                        required
                        />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder="Enter Description" 
                        
                        onChange= {e => setDescription(e.target.value)}
                        required
                        />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                            Add Record
                    </Button>

                </Form>
                </Card.Body>
            </Card>
            </Container>
        </React.Fragment> 
    )
}

export default update
