import React, { useContext, useState, useEffect} from 'react'
import {Form, Row, Col, Button, Card, Container} from 'react-bootstrap'
import View from '../../components/View';
import UserContext from '../../UserContext'

import AppHelper from '../../app-helper'


export default function create() {
    
    return ( 
        <View title={ 'Record' }>
            <Row className="justify-content-center">
                <Col xs md="12">
                    <h3 className="text-center">New Record</h3>
                    <NewRecord /> 
                </Col>
            </Row>
        </View>
    )
}   

const NewRecord = () => { 
    const [categories, setCategories] = useState('')
    const [categoryType, setCategoryType] = useState('') 
    const [categoryName, setCategoryName] = useState('') 
    const [amount, setAmount] = useState('') 
    const [description, setDescription] = useState ('')
        useEffect (() => {
            
            fetch(`https://intense-harbor-67272.herokuapp.com/api/users/details`,{
                headers: {
                    'Content-Type' : 'application/json',
                    Authorization : `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
                    console.log(data.categories)
                    console.log(categoryType)
                if(categoryType === "Expenses"){
                    
                    const categoryList = data.categories.map(category => {
                        
                        if(category.categoryType === "Expenses"){
                            return(
                                <option key={category._id}>{category.categoryName}</option>
                            )
                        }

                    })
                    setCategories(categoryList)
                }

                if(categoryType === "Income"){

                    const categoryList = data.categories.map(element => {

                        if(element.categoryType === "Income"){
                            return (
                                <option key={element._id}>{element.categoryName}</option>
                            )
                        }
                    })
                    setCategories(categoryList)
                }
            })

        },[categoryType])


        function addRecord(e) {
            e.preventDefault()
                fetch(`https://intense-harbor-67272.herokuapp.com/api/users/add-record`, {
                    "method": "POST",
                    "headers": {
                        'Content-Type' : 'application/json',
                        Authorization : `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        categoryType: categoryType,
                        categoryName: categoryName,
                        amount: amount,
                        description: description
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    alert("Sucessfully added in records")
            })
        }

                
        
        

        return (
        <React.Fragment>
            <Container className= "w-50">
            <Card>
                <Card.Body>
                <Form onSubmit={(e) => addRecord(e)}>
                    <Form.Group>
                        <Form.Label>Category Type</Form.Label>
                        <Form.Control as="select" required onChange={e => setCategoryType(e.target.value)}>
                        <option default>Select Category Type</option>
                        <option value= "Income">Income</option>
                        <option value = "Expenses">Expenses</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                        <Form.Label>Category Name</Form.Label>
                        <Form.Control as="select" onChange={e => setCategoryName (e.target.value)}>
                        <option default>Select Category Name</option>
                            {categories}
                        </Form.Control>
                    </Form.Group>

                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Amount</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder="Enter Amount" 
                        value= {amount}
                        onChange= {e => setAmount(e.target.value)}
                        required
                        />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder="Enter Description" 
                        value= {description}
                        onChange= {e => setDescription(e.target.value)}
                        required
                        />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                            Add Record
                    </Button>

                </Form>
                </Card.Body>
            </Card>
            </Container>
        </React.Fragment> 
        
        )
    }